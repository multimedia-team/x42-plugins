Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: X42-plugins
Upstream-Contact: Robin Gareus <robin@gareus.org>
Source: https://github.com/x42/x42-plugins
Comment: We use robtk from debian archive
Files-Excluded:
 robtk

Files: *
Copyright: 2013-2023 Robin Gareus <robin@gareus.org>
License: GPL-2+

Files:
 balance.lv2/pugl/pugl.h
 balance.lv2/pugl/pugl_internal.h
 balance.lv2/pugl/pugl_win.cpp
 balance.lv2/pugl/pugl_osx.m
Copyright: 2012 David Robillard <https://drobilla.net>
License: ISC

Files:
 balance.lv2/pugl/pugl_x11.c
Copyright: 2012 David Robillard <https://drobilla.net>
 2011-2012 Ben Loftis, Harrison Consoles
License: ISC

Files: dpl.lv2/src/peaklim.*
Copyright: 2010-2018 Fons Adriensen
License: GPL-3+

Files: debian/*
Copyright: 2013-2016 Jaromír Mikeš <mira.mikes@seznam.cz>
           2020-2024 Dennis Braun <snd@debian.org>
License: GPL-2+

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: GPL-3+
 This file is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 It is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with it.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License version 3
 can be found in the file `/usr/share/common-licenses/GPL-3'.
